import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, FlatList, StyleSheet } from 'react-native';
import { SearchBar, Avatar, Icon } from 'react-native-elements';


const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Roland Coronell',
    avatar: require('../../assets/images/icon.png'),
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Brandon Fierro',
    avatar: require('../../assets/images/icon.png'),
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Carlos Arce',
    avatar: require('../../assets/images/icon.png'),
  },
];

function Item({ title, avatar }) {
  return (
    <View style={styles.container}>
      <View style={{ width: '15%' }}>
      <Avatar
        size="medium"
        rounded
        title={title.substring(0,2)}
        onPress={() => console.log("Works!")}
        titleStyle={{ fontWeight: 'bold' }}
        overlayContainerStyle={{backgroundColor: '#0652DD'}}
      />
      </View>
      <View style={{ width: '100%' }}>
        <Text style={{ fontSize: 19, fontWeight: 'bold', marginLeft: 4 }}>{title}</Text>
      </View>
    </View>
  );
}

export default class Contacts extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: '#13C0CE',
    },
    headerLeft: (
      <View>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image
            style={{ marginLeft: 10, height: 25, width: 25 }}
            source={require('../../assets/images/icon.png')}
          />
        </TouchableOpacity>
      </View>
    ),
  });
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
  }

  render() {
    const { search } = this.state;
    return (
      <View style={{padding: 12, position: 'relative', height: '100%'}}>
        
        <View style={{ marginBottom: 10 }}>
          <Text style={{ fontSize: 26, fontWeight: 'bold' }}> Contactos </Text>
        </View>

        <View>
          <SearchBar
            placeholder="Buscar"
            onChangeText={this.updateSearch}
            value={search}
            lightTheme={true}
            round={true}
            inputContainerStyle={{
              backgroundColor: "#ecf0f1",
            }}
            containerStyle={{
              backgroundColor: "transparent", 
              marginBottom: 20,
              shadowColor: 'white', //no effect
              borderBottomColor: 'transparent',
              borderTopColor: 'transparent'
            }}
          />
        </View>

        <SafeAreaView >
          <FlatList
            data={DATA}
            renderItem={({ item }) => <Item title={item.title}
                                            avatar={item.avatar} />}
            keyExtractor={item => item.id}
          />
        </SafeAreaView>

        <Icon
          name='add-circle'
          type='material'
          color='#0652DD'
          size={65}
          containerStyle={{position: 'absolute', bottom: 25, right: 18}}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 22,
  },
  
  avatar: {
     width: 50,
     height: 50,
     borderRadius: 50 / 2
    }
});