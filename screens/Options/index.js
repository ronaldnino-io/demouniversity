import React from 'react';
import {Text, View} from 'react-native';
import {styles} from './styles';


export default function Options() {
  return (
    <View style={styles.container}>
        <Text style={styles.text}>Options</Text>
    </View>
  );
}

Options.navigationOptions = {
  title: 'Options',
};