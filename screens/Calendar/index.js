import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, SafeAreaView, FlatList, StyleSheet } from 'react-native';
import Constants from 'expo-constants';


const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];


function Item({ title }) {
  return (
    <View style={styles.item}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default class Calendar extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      backgroundColor: '#13C0CE',
    },
    headerLeft: (
      <View>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Image
            style={{ marginLeft: 10, height: 25, width: 25 }}
            source={require('../../assets/images/icon.png')}
          />
        </TouchableOpacity>
      </View>
    ),
  });
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>

        <View style={{
            flexDirection: 'row',
            alignItems: 'center',
            height: 100,
            padding: 20,
          }}>
          <Image
            style={{width: 50, height: 50}}
            source={require('../../assets/images/icon.png')}
          />
          <View>
            <Text style={{ fontSize: 20 }}> Chat </Text>
            <Text style={{ fontSize: 20 }}> Chat </Text>
          </View>
        </View>
        
        <View>
          <SafeAreaView style={styles.container}>
            <FlatList
              data={DATA}
              renderItem={({ item }) => <Item title={item.title} />}
              keyExtractor={item => item.id}
            />
          </SafeAreaView>
        </View>
        
        
      </View>
     
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});