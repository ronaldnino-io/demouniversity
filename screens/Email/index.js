import React from 'react';
import {Text, View} from 'react-native';
import {styles} from './styles';


export default function Email() {
  return (
    <View style={styles.container}>
        <Text style={styles.text}>Email</Text>
    </View>
  );
}

Email.navigationOptions = {
  title: 'Email',
};