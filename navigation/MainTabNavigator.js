import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/Home/HomeScreen';
import ContactsScreen from '../screens/Contacts';
import ChatScreen from '../screens/Chat';
import CalendarScreen from "../screens/Calendar";
import EmailScreen from "../screens/Email";
import OptionsScreen from "../screens/Options";

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-contacts${focused ? '' : '-outline'}`
          : 'md-contacts'
      }
    />
  ),
};

HomeStack.path = '';


const Contacts = createStackNavigator(
  {
    Contacs: ContactsScreen,
  },
  config
);

Contacts.navigationOptions = {
  tabBarLabel: 'Contacts',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-contacts${focused ? '' : '-outline'}`
          : 'md-contacts'
      }
    />
  ),
};

Contacts.path = '';


const Chat = createStackNavigator(
  {
    Contacs: ChatScreen,
  },
  config
);

Chat.navigationOptions = {
  tabBarLabel: 'Chat',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-chatbubbles${focused ? '' : '-outline'}`
          : 'md-chatboxes'
      }
    />
  ),
};

Chat.path = '';

const Calendar = createStackNavigator(
  {
    Contacs: CalendarScreen,
  },
  config
);

Calendar.navigationOptions = {
  tabBarLabel: 'Calendar',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-calendar${focused ? '' : '-outline'}`
          : 'md-calendar'
      }
    />
  ),
};

Calendar.path = '';

const Email = createStackNavigator(
  {
    Contacs: EmailScreen,
  },
  config
);

Email.navigationOptions = {
  tabBarLabel: 'Email',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-mail${focused ? '' : '-outline'}`
          : 'md-mail'
      }
    />
  ),
};

Email.path = '';



const Opcion = createStackNavigator(
  {
    Settings: OptionsScreen,
  },
  config
);

Opcion.navigationOptions = {
  tabBarLabel: 'Options',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? 'ios-apps' : 'md-apps'} />
  ),
};

Opcion.path = '';

const tabNavigator = createBottomTabNavigator({
  Contacts,
  Chat,
  Calendar,
  Email,
  Opcion
});

tabNavigator.path = '';

export default tabNavigator;
