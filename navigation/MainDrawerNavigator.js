import { createDrawerNavigator, DrawerItems } from 'react-navigation';
import MainTabNavigator from './MainTabNavigator';
import ContactsScreen from '../screens/Contacts';
import CalendarScreen from '../screens/Calendar';
import React from 'react';
import {StyleSheet, Text, View, SafeAreaView, ScrollView, Dimensions, Image } from 'react-native';




const CustomDrawerContentComponent = props => (
     
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
       <View style={{ height: 250, backgroundColor: '#d2d2d2', opacity: 0.9 }}>
        <View style={{ height: 200, backgroundColor: 'Green', alignItems: 'center', justifyContent: 'center' }}>
          <Image source={require('../assets/no-image.png')} style={{ height: 150, width: 150, borderRadius: 60 }} />
        </View>
        <View style={{ height: 50, backgroundColor: 'Green', alignItems: 'center', justifyContent: 'center' }}>
          <Text>John Doe</Text>
        </View>
      </View>
      <ScrollView>
        <DrawerItems {...props} />
      </ScrollView>
    </SafeAreaView>
  
  );
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      
    },
  });

const RouteConfigs = {
    MainTabNavigator: { 
            screen: MainTabNavigator,
        },
        Contacts: {
        screen: ContactsScreen,
        },
        Calendar: {
        screen: CalendarScreen,
        }
    };

    const DrawerNavigatorConfig = {
        navigationOptions: {
             drawerLockMode: 'locked-closed'
        },
        contentComponent: CustomDrawerContentComponent
    }

const MenuDrawer = createDrawerNavigator(RouteConfigs,DrawerNavigatorConfig);

export default MenuDrawer;    