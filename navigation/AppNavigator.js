import { createAppContainer, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import MenuDrawer from './MainDrawerNavigator';

export default createAppContainer(
  createSwitchNavigator({
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    Main: MenuDrawer,
  })
);
